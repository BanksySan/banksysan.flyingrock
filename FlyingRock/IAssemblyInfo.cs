﻿namespace BanksySan.FlyingRock
{
    using System.Reflection;

    public interface IAssemblyInfo
    {
        AssemblyName Name { get; }
    }
}