namespace BanksySan.FlyingRock
{
    using System.IO;

    internal interface IArgument
    {
        FileInfo MsBuildExecutable { get; }
    }
}