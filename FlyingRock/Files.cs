﻿namespace BanksySan.FlyingRock
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    public class Files
    {
        private readonly TextWriter _out;

        public Files(TextWriter output = null)
        {
            _out = output ?? new StreamWriter(Stream.Null);
        }

        public IEnumerable<FileInfo> FindProjects(DirectoryInfo rootDirectory, Func<FileSystemInfo, bool> filter = null)
        {
            var fileSystemInfos = rootDirectory.GetFileSystemInfos("*.csproj", SearchOption.AllDirectories).ToArray();
            fileSystemInfos = filter == null ? fileSystemInfos : fileSystemInfos.Where(filter).ToArray();

            if (fileSystemInfos.Any(info => info.Attributes.HasFlag(FileAttributes.Directory) ||
                                            !string.Equals(info.Extension, ".csproj")))
            {
                var errorMessage = new StringBuilder("Looks like not all the files are project files:");

                foreach (var fileSystemInfo in fileSystemInfos)
                {
                    var isCsProj = fileSystemInfo.Attributes.HasFlag(FileAttributes.Directory) ||
                                   !string.Equals(fileSystemInfo.Extension, "*.csproj");

                    if (isCsProj)
                    {
                        errorMessage.AppendLine(fileSystemInfo.FullName);
                    }
                }
                throw new Exception(errorMessage.ToString());
            }


            for (var i = 0; i < fileSystemInfos.Length; i++)
            {
                _out.WriteLine($"{i,5} {fileSystemInfos[i].FullName}");
            }

            return fileSystemInfos.Cast<FileInfo>();
        }
    }
}