namespace BanksySan.FlyingRock
{
    using System;
    using System.Collections.Generic;

    public interface IAssemblyVersions
    {
        string Name { get; }
        IEnumerable<Version> Versions { get; }
    }
}