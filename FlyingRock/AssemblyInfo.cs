namespace BanksySan.FlyingRock
{
    using System.Reflection;

    internal class AssemblyInfo : IAssemblyInfo
    {
        public AssemblyInfo(AssemblyName name)
        {
            Name = name;
        }

        public AssemblyName Name { get; }
    }
}