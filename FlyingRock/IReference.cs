namespace BanksySan.FlyingRock
{
    using System.IO;
    using System.Reflection;

    public interface IReference
    {
        AssemblyName AssemblyName { get; }
        FileInfo HintPath { get; }
        bool Private { get; }
        bool SpecificVersion { get; }
    }
}