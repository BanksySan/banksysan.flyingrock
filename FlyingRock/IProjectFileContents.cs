namespace BanksySan.FlyingRock
{
    using System.Collections.Generic;

    public interface IProjectFileContents
    {
        string AssemblyName { get; }
        string AssemblyType { get; }
        string OutputPath { get; }
        string RootNamespace { get; }
        string TargetFramework { get; }
        string KeyFilePath { get; }
        bool IsStronglyTyped { get; }
        IEnumerable<string> CompileItems { get; }
        IEnumerable<IReference> References { get; }
    }
}