﻿namespace BanksySan.FlyingRock
{
    using System.IO;

    public class Project : IProject
    {
        private static readonly IAssemblyExtensions EXTENSIONS = new AssemblyExtensions();

        public Project(FileInfo projectFile, IProjectFileContents fileContents)
        {
            ProjectFile = projectFile;
            FileContents = fileContents;

            var fileName = $"{FileContents.AssemblyName}.{EXTENSIONS[FileContents.AssemblyType]}";
            var fullOutputFilePath = $"{projectFile.DirectoryName}\\{fileContents.OutputPath}\\{fileName}";
            OutputFile = new FileInfo(Path.GetFullPath(fullOutputFilePath));
        }

        public FileInfo ProjectFile { get; }
        public IProjectFileContents FileContents { get; }
        public FileInfo OutputFile { get; }
    }
}