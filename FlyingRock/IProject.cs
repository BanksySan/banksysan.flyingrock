﻿namespace BanksySan.FlyingRock
{
    using System.IO;

    public interface IProject
    {
        FileInfo ProjectFile { get; }
        IProjectFileContents FileContents { get; }
        FileInfo OutputFile { get; }
    }
}