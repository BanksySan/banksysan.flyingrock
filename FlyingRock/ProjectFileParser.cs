﻿namespace BanksySan.FlyingRock
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Xml;
    using System.Xml.XPath;

    internal class ProjectFileParser
    {
        private readonly FileInfo _projectFileInfo;

        public ProjectFileParser(FileInfo projectFileInfo)
        {
            _projectFileInfo = projectFileInfo ?? throw new ArgumentNullException(nameof(projectFileInfo));
        }

        public IProjectFileContents Parse()
        {
            using (var streamReader = _projectFileInfo.OpenText())
            {
                using (var xmlReader = XmlReader.Create(streamReader))
                {
                    var document = new XPathDocument(xmlReader);
                    var navigator = document.CreateNavigator();
                    XmlNamespaceManager namespaceManager = null;

                    if (navigator.NameTable != null)
                    {
                        namespaceManager = new XmlNamespaceManager(navigator.NameTable);
                    }

                    if (namespaceManager == null)
                    {
                        throw new
                            FormatException("The file doesn't have a namespace table and so is unlikely to be a build file.");
                    }

                    const string NAMESPACE = "ns";

                    namespaceManager.AddNamespace(NAMESPACE, "http://schemas.microsoft.com/developer/msbuild/2003");


                    var outputPath = navigator
                        .SelectSingleNode($"/{NAMESPACE}:Project/{NAMESPACE}:PropertyGroup/{NAMESPACE}:OutputPath",
                                          namespaceManager)?.Value;
                    var assemblyType = navigator
                        .SelectSingleNode($"/{NAMESPACE}:Project/{NAMESPACE}:PropertyGroup/{NAMESPACE}:OutputType",
                                          namespaceManager)?.Value;
                    var assemblyName = navigator
                        .SelectSingleNode($"/{NAMESPACE}:Project/{NAMESPACE}:PropertyGroup/{NAMESPACE}:AssemblyName",
                                          namespaceManager)?.Value;
                    var rootNamespace = navigator
                        .SelectSingleNode($"//{NAMESPACE}:Project/{NAMESPACE}:PropertyGroup/{NAMESPACE}:RootNamespace",
                                          namespaceManager)?.Value;
                    var targetFramework = navigator
                        .SelectSingleNode($"/{NAMESPACE}:Project/{NAMESPACE}:PropertyGroup/{NAMESPACE}:TargetFrameworkVersion",
                                          namespaceManager)?.Value;
                    var keyFileNode = navigator
                        .SelectSingleNode($"/{NAMESPACE}:Project/{NAMESPACE}:PropertyGroup/{NAMESPACE}:AssemblyOriginatorKeyFile",
                                          namespaceManager)?.Value;
                    var isStronglyTyped =
                        navigator
                            .SelectSingleNode($"/{NAMESPACE}:Project/{NAMESPACE}:PropertyGroup/{NAMESPACE}:SignAssembly",
                                              namespaceManager)?.ValueAsBoolean ?? false;

                    var itemGroupIterator =
                        navigator.Select($"/{NAMESPACE}:Project/{NAMESPACE}:ItemGroup/*", namespaceManager);

                    var references = new List<IReference>();
                    var compileItems = new List<string>();

                    while (itemGroupIterator.MoveNext())
                    {
                        var item = itemGroupIterator.Current;

                        switch (item.Name)
                        {
                            case "Reference":

                                var referenceName = item.SelectSingleNode("@Include").Value;
                                var isSpecificVersion =
                                    item.SelectSingleNode($"{NAMESPACE}:SpecificVersion", namespaceManager)?.Value
                                        .Equals("true", StringComparison.InvariantCultureIgnoreCase) == true;
                                var isPrivate =
                                    item.SelectSingleNode($"{NAMESPACE}:Private", namespaceManager)?.Value
                                        .Equals("true", StringComparison.InvariantCultureIgnoreCase) == true;

                                var hintPathRelative = item
                                    .SelectSingleNode($"{NAMESPACE}:HintPath", namespaceManager)?.Value;

                                FileInfo hintPath = null;

                                if (hintPathRelative != null && !Path.IsPathRooted(hintPathRelative))
                                {
                                    var fullHintPath =
                                        Path.GetFullPath($@"{_projectFileInfo.Directory.FullName}\{hintPathRelative}");

                                    hintPath = new FileInfo(fullHintPath);

                                }

                                var referenceAssemblyName =
                                    referenceName == null ? null : new AssemblyName(referenceName);

                                references.Add(new Reference(referenceAssemblyName,
                                                             hintPath,
                                                             isPrivate,
                                                             isSpecificVersion));
                                break;


                            case "Compile":

                                var compileName = item.SelectSingleNode("//@Include").Value;

                                compileItems.Add(compileName);
                                break;
                        }
                    }


                    return new ProjectFileContents(assemblyName,
                                                   assemblyType,
                                                   outputPath,
                                                   rootNamespace,
                                                   targetFramework,
                                                   keyFileNode,
                                                   isStronglyTyped,
                                                   compileItems,
                                                   references);
                }
            }
        }
    }
}