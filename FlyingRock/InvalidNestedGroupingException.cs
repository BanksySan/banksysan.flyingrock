namespace BanksySan.FlyingRock
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;

    [ComVisible(true)]
    [Serializable]
    public class InvalidNestedGroupingException : InvalidOperationException
    {
        public InvalidNestedGroupingException(string message) : base(message)
        {
        }

        public InvalidNestedGroupingException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InvalidNestedGroupingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}