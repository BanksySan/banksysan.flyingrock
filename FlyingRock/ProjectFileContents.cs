namespace BanksySan.FlyingRock
{
    using System.Collections.Generic;

    internal class ProjectFileContents : IProjectFileContents
    {
        public ProjectFileContents(string assemblyName,
                                   string assemblyType,
                                   string outputPath,
                                   string rootNamespace,
                                   string targetFramework,
                                   string keyFilePath,
                                   bool isStronglyTyped,
                                   IEnumerable<string> compileItems,
                                   IEnumerable<IReference> references)
        {
            AssemblyName = assemblyName;
            AssemblyType = assemblyType;
            OutputPath = outputPath;
            RootNamespace = rootNamespace;
            TargetFramework = targetFramework;
            KeyFilePath = keyFilePath;
            IsStronglyTyped = isStronglyTyped;
            CompileItems = compileItems;
            References = references;
        }

        public string AssemblyName { get; }
        public string AssemblyType { get; }
        public string OutputPath { get; }
        public string RootNamespace { get; }
        public string TargetFramework { get; }
        public string KeyFilePath { get; }
        public bool IsStronglyTyped { get; }
        public IEnumerable<string> CompileItems { get; }
        public IEnumerable<IReference> References { get; }
    }
}