﻿namespace BanksySan.FlyingRock
{
    using System;

    internal class BuildTime : IBuildTime
    {
        public BuildTime(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }

        public DateTime Start { get; }
        public DateTime End { get; }
    }
}