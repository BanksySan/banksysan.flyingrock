namespace BanksySan.FlyingRock
{
    using System;
    using System.Collections.Generic;

    public class AssemblyVersions : IAssemblyVersions
    {
        public AssemblyVersions(string assemblyName, IEnumerable<Version> versions)
        {
            Name = assemblyName;
            Versions = versions;
        }

        public string Name { get; }
        public IEnumerable<Version> Versions { get; }
    }
}