namespace BanksySan.FlyingRock
{
    internal class BuildDetails : IBuildDetails
    {
        public BuildDetails(int exitCode)
        {
            ExitCode = exitCode;
        }

        public int ExitCode { get; }
    }
}