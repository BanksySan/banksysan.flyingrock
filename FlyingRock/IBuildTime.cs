﻿namespace BanksySan.FlyingRock
{
    using System;

    public interface IBuildTime
    {
        DateTime Start { get; }
        DateTime End { get; }
    }
}