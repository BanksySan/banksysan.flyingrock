﻿namespace BanksySan.FlyingRock
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using NodeSpace;

    public class Application
    {
        private const int NOT_RUN_EXIT_CODE = -1;
        private static readonly IAssemblyExtensions EXTENSIONS = new AssemblyExtensions();
        private readonly FileInfo _msBuildFile;

        public Application(DirectoryInfo rootDirectory, FileInfo msBuildFile, Func<FileSystemInfo, bool> filter = null)
        {
            _msBuildFile = msBuildFile;
            var projectFiles = new Files().FindProjects(rootDirectory, filter).ToArray();
            var projects = new List<IProject>();

            foreach (var file in projectFiles)
            {
                var parser = new ProjectFileParser(file);
                var projectDetails = parser.Parse();
                projects.Add(new Project(file, projectDetails));
            }

            Projects = projects;
        }

        public IEnumerable<IProject> Projects { get; }

        public IEnumerable<AssemblyName> ExpectedGacAssemblies
        {
            get
            {
                var assemblies = new HashSet<AssemblyName>();
                var assemblyNames = new HashSet<string>();

                foreach (var project in Projects)
                {
                    foreach (var reference in project.FileContents.References)
                    {
                        if (reference.HintPath == null && assemblyNames.Add(reference.AssemblyName.FullName))
                        {
                            assemblies.Add(reference.AssemblyName);
                        }
                    }
                }

                return assemblies;
            }
        }


        public IEnumerable<IProject> TopLevelProjects
        {
            get
            {
                var hintPaths = Projects.SelectMany(x => x.FileContents.References.Select(y => y.HintPath))
                    .Where(x => x != null);

                var topLevelProtects = Projects.Where(project =>
                {
                    var outputAssemblyName =
                        $"{project.FileContents.AssemblyType}.{EXTENSIONS[project.FileContents.AssemblyType]}";

                    var outputFile =
                        Path.GetFullPath(
                            $@"{project.ProjectFile.FullName}\{project.FileContents.OutputPath}\{outputAssemblyName}");

                    foreach (var hintPath in hintPaths)
                    {
                        var hintPathFullName = Path.GetFullPath(hintPath.FullName);
                        if (hintPathFullName == outputFile)
                        {
                            return false;
                        }
                    }

                    return true;
                });

                return topLevelProtects;
            }
        }

        private IEnumerable<INode<INodeState, TInvocation, TResponse>> NodeGraph<TInvocation, TResponse>(
            IInvocationSettings<INodeState, TInvocation, TResponse> invocationSetting)
        {
            var projectNodes = new HashSet<Node<INodeState, TInvocation, TResponse>>();

            foreach (var project in Projects)
            {
                var node = new Node<INodeState, TInvocation, TResponse>(project.FileContents.AssemblyName,
                    new NodeState {ProjectInfo = project},
                    invocationSetting);
                projectNodes.Add(node);
            }

            foreach (var projectNode in projectNodes)
            {
                foreach (var reference in projectNode.Data.ProjectInfo.FileContents.References)
                {
                    var referenceNode =
                        projectNodes.SingleOrDefault(x => x.Data.ProjectInfo.OutputFile.FullName ==
                                                          reference.HintPath?.FullName);
                    if (referenceNode != null)
                    {
                        projectNode.AddLink(referenceNode);
                    }
                }
            }

            return projectNodes;
        }

        public IEnumerable<IAssemblyVersions> GetAssemblyVersions()
        {
            var assemblies = new List<AssemblyName>();
            var versionsByAssemblyName = new List<IAssemblyVersions>();

            foreach (var project in Projects)
            {
                var references = project.FileContents.References.Select(x => x.AssemblyName).Distinct();
                assemblies.AddRange(references);
            }

            foreach (var assembly in assemblies.GroupBy(assembly => assembly.Name))
            {
                var versions = assembly.Select(x => x.Version);

                versionsByAssemblyName.Add(new AssemblyVersions(assembly.Key, versions));
            }

            return versionsByAssemblyName;
        }

        public void PerformBuild(IProject project)
        {
            PerformBuild(new[] {project});
        }

        public void PerformBuild(IEnumerable<IProject> projects = null)
        {
            var invocationSetting =
                new InvocationSettings<INodeState, IArgument, IBuildDetails>((node, childResponses, argument) =>
                {
                    lock (node)
                    {
                        var buildProcess = node.Data.BuildProcess;

                        if (buildProcess != null)
                        {
                            buildProcess.WaitForExit();
                            return new BuildDetails(buildProcess.ExitCode);
                        }


                        if (childResponses.Any(response => response.Response.ExitCode != 0))
                        {
                            Console.WriteLine("Skipping build as children failed.");
                            return new BuildDetails(NOT_RUN_EXIT_CODE);
                        }

                        var processInfo = new ProcessStartInfo
                        {
                            Arguments = node.Data.ProjectInfo.ProjectFile.FullName,
                            CreateNoWindow = true,
                            FileName = _msBuildFile.FullName,
                            RedirectStandardError = false,
                            RedirectStandardOutput = false,
                            UseShellExecute = true,
                            WorkingDirectory = argument.MsBuildExecutable.DirectoryName
                        };


                        node.Data.BuildProcess = new Process {StartInfo = processInfo};
                        node.Data.BuildProcess.Start();
                        Debug.WriteLine($"Started: {node.Name,-50}");
                        node.Data.BuildProcess.WaitForExit();

                        Console.WriteLine($"{node.Data.BuildProcess.ExitCode,10}:{node.Name}");

                        return new BuildDetails(node.Data.BuildProcess.ExitCode);
                    }
                });

            var nodeGraph = NodeGraph(invocationSetting).ToArray();

            if (projects == null)
            {
                foreach (var node in nodeGraph)
                {
                    Debug.WriteLine($"Invoke: {node.Name,-50}");
                    node.Invoke(new BuildArgument(_msBuildFile));
                }
            }
            else
            {
                foreach (var project in projects)
                {
                    var node = nodeGraph.Single(n => ReferenceEquals(n.Data.ProjectInfo, project));
                    node.Invoke(new BuildArgument(_msBuildFile));
                }
            }
        }
    }
}