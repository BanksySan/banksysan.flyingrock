namespace BanksySan.FlyingRock
{
    public interface IBuildDetails
    {
        int ExitCode { get; }
    }
}