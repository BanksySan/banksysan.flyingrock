namespace BanksySan.FlyingRock
{
    using System.Diagnostics;

    internal interface INodeState
    {
        IProject ProjectInfo { get; }
        Process BuildProcess { get; set; }
    }
}