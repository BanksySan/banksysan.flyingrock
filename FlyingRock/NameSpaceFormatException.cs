namespace BanksySan.FlyingRock
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;

    [ComVisible(true)]
    [Serializable]
    public class NameSpaceFormatException : FormatException
    {
        public NameSpaceFormatException(string nameSpace) : base(nameSpace)
        {
        }

        public NameSpaceFormatException(string nameSpace, Exception innerException) :
            base($"'{nameSpace}' is not a valid namespace.", innerException)
        {
        }

        public NameSpaceFormatException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}