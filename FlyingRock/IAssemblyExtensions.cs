namespace BanksySan.FlyingRock
{
    public interface IAssemblyExtensions
    {
        string this[string type] { get; }
    }
}