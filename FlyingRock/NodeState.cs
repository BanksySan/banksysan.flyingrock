namespace BanksySan.FlyingRock
{
    using System.Diagnostics;

    internal class NodeState : INodeState
    {
        public IProject ProjectInfo { get; set; }
        public Process BuildProcess { get; set; }
    }
}