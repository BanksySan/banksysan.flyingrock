﻿namespace BanksySan.FlyingRock
{
    using System.Collections.Generic;

    internal class AssemblyExtensions : IAssemblyExtensions
    {
        private static readonly IDictionary<string, string> ASSEMBLY_EXTENSIONS =
            new Dictionary<string, string> {{"Library", "dll"}, {"Exe", "exe"}, {"WinExe", "exe"}};

        public string this[string type] => ASSEMBLY_EXTENSIONS[type];
    }
}