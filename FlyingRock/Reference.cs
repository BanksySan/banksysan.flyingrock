﻿namespace BanksySan.FlyingRock
{
    using System;
    using System.IO;
    using System.Reflection;

    public class Reference : IReference
    {
        public Reference(AssemblyName assemblyName, FileInfo hintPath, bool isPrivate, bool specificVersion)
        {
            AssemblyName = assemblyName ?? throw new ArgumentNullException(nameof(assemblyName));
            HintPath = hintPath;
            Private = isPrivate;
            SpecificVersion = specificVersion;
        }

        public FileInfo HintPath { get; private set; }

        public AssemblyName AssemblyName { get; private set; }

        public bool Private { get; private set; }

        public bool SpecificVersion { get; private set; }
    }
}