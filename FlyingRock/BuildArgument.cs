namespace BanksySan.FlyingRock
{
    using System.IO;

    internal class BuildArgument : IArgument
    {
        public BuildArgument(FileInfo buildExecutable)
        {
            MsBuildExecutable = buildExecutable;
        }

        public FileInfo MsBuildExecutable { get; }
    }
}